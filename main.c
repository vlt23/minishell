#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <malloc.h>
#include "parser.h"
#include <signal.h>
#include <fcntl.h>

// Funciones
void redireccion1();
void redireccion2(char *nombre, int fd);

// Variables globales
char prompt[] = "msh> ";
char fin[] = "exit\n";          // para finalizar el minishell

tline* line;
pid_t hijosBackGround[128];
int numDeHijo;

int fdInput, fdOutput, fdError;

int main(void)
{
    // Evitar que el minishell muera
    if (signal(SIGINT, SIG_IGN) == SIG_ERR)
    {
        exit(EXIT_FAILURE);
    }
    if (signal(SIGQUIT, SIG_IGN) == SIG_ERR)
    {
        exit(EXIT_FAILURE);
    }

    fdInput = dup(STDIN_FILENO);
    fdOutput = dup(STDOUT_FILENO);
    fdError = dup(STDERR_FILENO);

    int status;
    char buffer[1024];
    char *ruta = getenv("HOME");
    getcwd(ruta, -1);
    int contador = 0;
    int bufferComandos[1024][1024];     // almacenar comandos en el buffer
                // 1 posicion es el numero de comando, 2 es la expresion del comando

    int True = 1;         // mientras no introduzca 'exit'
    while (True)
    {
        printf("%s ", ruta);
        printf("msh> ");
        fgets(buffer, 1024, stdin);

        if (strcmp(buffer, fin) == 0)   // se ha introducido 'exit'
        {
            True = 0;                 // salir del bucle
        }
        // ------------------------------- cd ------------------------------------
        else if (buffer[0] == 'c' && buffer[1] == 'd')
        {
            // ir a $HOME
            if (buffer[2] == '\n')
            {
                char* home = getenv("HOME");
                if (chdir(home) != 0)
                {
                    fprintf(stderr, "El sistema no tiene la variable $HOME\n");
                }
            }
            else if (buffer[2] == ' ')      // por si se introduce cdDocumentos
            {                               // en vez de cd Documentos
                unsigned i;
                char *dir = (char *) malloc(sizeof(char) * (strlen(buffer) - 3));
                for (i = 0; i < (strlen(buffer) - 4); i++)
                {
                    dir[i] = buffer[i + 3];
                }
                if (chdir(dir) != 0)
                {
                    fprintf(stderr, "No existe el directorio\n");
                }
                free(dir);
            }
            getcwd(ruta, -1);
        }
        // ----------------------------- fin cd ---------------------------------
        // ------------------------------- fg -----------------------------------
        else if (buffer[0] == 'f' && buffer[1] == 'g')
        {
            // fg sin argumentos
            if (buffer[2] == '\n')
            {
                if (hijosBackGround[contador - 1] > 0)
                {
                    waitpid(hijosBackGround[contador - 1], NULL, 0);
                    hijosBackGround[contador - 1] = -1;
                }
                else
                {
                    printf("No existe ese trabajo\n");
                }
            }
            // fg con argumentos
            else if (buffer[2] == ' ')
            {
                char *arg = (char *) malloc(sizeof(char) * strlen(buffer - 4));
                for (unsigned i = 0; i < strlen(buffer) - 4; i++)
                {
                    arg[i] = buffer[i + 3];
                }
                if (hijosBackGround[atoi(arg)] > 0)
                {
                    waitpid(hijosBackGround[atoi(arg)], NULL, 0);
                    hijosBackGround[atoi(arg)] = -1;
                }
                else
                {
                    printf("No existe ese trabajo\n");
                }
                free(arg);
            }
        }
        // ------------------------------ fin fg -------------------------------------
        // ------------------------------- jobs -------------------------------------
        else if (buffer[0] == 'j' && buffer[1] == 'o' && buffer[2] == 'b' && buffer[3] == 's'
                 && buffer[4] == '\n')
        {
            for (int i = 0; i < contador; i++)
            {
                if (hijosBackGround[i] != -1)
                {
                    printf("[%d]+ Ejecutando    %ls", i, bufferComandos[i]);
                }
            }
        }
        // ----------------------------- fin jobs -------------------------------------
        else
        {
            line = tokenize(buffer);
            // si no se introduce nada
            if (line->ncommands == 0)
            {
                printf("\n");
            }
            // un solo mandato (sin pipes)
            else if (line->ncommands == 1)
            {
                numDeHijo = 0;
                pid_t pid = fork();

                if (pid == 0)
                {
                    redireccion1();
                    execvp(line->commands[numDeHijo].filename, line->commands[numDeHijo].argv);
                }

                if (pid != 0)           // proceso padre
                {
                    if (line->background == 1)
                    {
                        waitpid(pid, NULL, WNOHANG);
                        hijosBackGround[0] = line->ncommands - 1;
                    }
                    else
                    {
                        waitpid(pid, &status, 0);
                        if (WIFEXITED(status) != 0)
                        {
                            if (WEXITSTATUS(status) != 0)
                            {
                                printf("El comando no se ejecutó correctamente\n");
                            }
                        }
                    }
                }
            }
            // varios mandatos
            else
            {
                pid_t pid;
                int p[2];
                int numHijo;
                int fdAux;

                for (numHijo = 0; numHijo < line->ncommands; numHijo++)
                {
                    if (numHijo < (line->ncommands - 1))
                    {
                        pipe(p);        // creando pipes
                    }
                    dup2(p[0], STDIN_FILENO);
                    dup2(p[1], STDOUT_FILENO);
                    // primer hijo
                    if (numHijo == 0)
                    {
                        fdAux = dup(STDIN_FILENO);
                        if (line->redirect_input != NULL)
                        {
                            redireccion2(line->redirect_input, STDIN_FILENO);
                        }
                        else
                        {
                            dup2(fdInput, STDIN_FILENO);
                        }
                    }
                    // hijos intermedios
                    else if ((numHijo > 0) && (numHijo < (line->ncommands - 1)))
                    {
                        dup2(fdAux, STDIN_FILENO);
                        fdAux = dup(p[0]);
                    }
                    // ultimo hijo
                    else
                    {
                        dup2(fdAux, STDIN_FILENO);
                        if (line->redirect_output != NULL)
                        {
                            // redirección salida estándar
                            redireccion2(line->redirect_output, STDOUT_FILENO);
                        }
                        else if (line->redirect_error != NULL)
                        {
                            // redirección salida de error
                            redireccion2(line->redirect_error, STDERR_FILENO);
                        }
                        else
                        {
                            dup2(fdOutput, STDOUT_FILENO);
                        }
                    }
                    // cerrar pipes, ya no son necesarios
                    close(p[0]);
                    close(p[1]);

                    pid = fork();
                    if (pid < 0)
                    {
                        fprintf(stderr, "Error en el fork()\n");
                        exit(1);
                    }
                    if (pid == 0)
                    {
                        close(fdAux);
                        execvp(line->commands[numHijo].filename, line->commands[numHijo].argv);
                        fprintf(stderr, "%s: No se encuentra el mandato\n", line->commands[numHijo].argv[0]);
                        exit(1);
                    }
                    else
                    {
                        if (line->background == 1)
                        {
                            waitpid(pid, NULL, WNOHANG);
                        }
                        else
                        {
                            waitpid(pid, NULL, 0);
                        }
                    }
                }
            }
            dup2(fdInput, STDIN_FILENO);
            dup2(fdOutput, STDOUT_FILENO);
            dup2(fdError, STDERR_FILENO);
        }
    }
    return 0;
}


void redireccion2(char* nombre, int fd)
{
    int fdAux;

    if (fd == STDIN_FILENO)
    {
        fdAux = open(nombre, O_RDONLY);
    }
    else
    {
        fdAux = open(nombre, O_WRONLY | O_CREAT | O_TRUNC);
    }

    if (fdAux == -1)
    {
        fprintf(stderr, "%s: Error. %s\n", nombre, strerror(errno));
    }

    if (dup2(fdAux, fd) == -1)
    {
        fprintf(stderr, "Se ha producido un error en la redirección\n");
    }
}

void redireccion1()
{
    if (numDeHijo == line->ncommands - 1)
    {
        if (line->redirect_error != NULL)
        {
            int fdError = open(line->redirect_error, O_CREAT | O_RDWR);
            if (fdError == -1)
            {
                fprintf(stderr, "%s: Error. %s\n", line->redirect_error, strerror(errno));
            }
            else
            {
                dup2(fdError, 2);
            }
        }
        else if (line->redirect_output != NULL)
        {
            int fdSalida = open(line->redirect_output, O_CREAT | O_RDWR);
            if (fdSalida == -1)
            {
                fprintf(stderr, "%s: Error. %s\n", line->redirect_output, strerror(errno));
            }
            else
            {
                dup2(fdSalida, 1);
            }
        }
    }
    else if (numDeHijo == 0)
    {
        if (line->redirect_input != NULL)
        {
            int fdEntrada = open(line->redirect_input, O_RDWR);
            if (fdEntrada == -1)
            {
                fprintf(stderr, "%s: Error. %s\n", line->redirect_input, strerror(errno));
            }
            else
            {
                dup2(fdEntrada, 0);
            }
        }
    }
}
